import React, { useRef } from "react";
import { useFrame } from "@react-three/fiber";
import * as THREE from "three";

const Stars = () => {
  const stars = useRef();
  const starGeo = new THREE.BufferGeometry();
  const starsVertices = [];

  for (let i = 0; i < 6000; i++) {
    const star = new THREE.Vector3(
      Math.random() * 600 - 300,
      Math.random() * 600 - 300,
      Math.random() * 600 - 300
    );
    star.velocity = 2;
    star.acceleration = 2;
    starsVertices.push(star.x, star.y, star.z);
  }

  starGeo.setAttribute(
    "position",
    new THREE.Float32BufferAttribute(starsVertices, 3)
  );

  const sprite = new THREE.TextureLoader().load("images/star.png");
  const starMaterial = new THREE.PointsMaterial({
    color: 0xaaaaaa,
    size: 0.7,
    map: sprite,
  });

  console.log(starMaterial);

  useFrame(() => {
    starGeo.attributes.position.array.forEach((_, index) => {
      if (index % 3 === 2) {
        starGeo.attributes.position.array[index] += 0.1;
        if (starGeo.attributes.position.array[index] > 200) {
          starGeo.attributes.position.array[index] = -200;
        }
      }
    });
    starGeo.attributes.position.needsUpdate = true;
    stars.current.rotation.y += 0.002;
  });

  return (
    <points ref={stars}>
      <primitive object={new THREE.Points(starGeo, starMaterial)} />
    </points>
  );
};

export default Stars;

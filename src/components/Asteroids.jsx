import { useGLTF } from "@react-three/drei";
import { useFrame } from "@react-three/fiber";
import React, { useRef } from "react";
import { fadeOnBeforeCompile } from "../utils/fadeMaterial";

export function Asteroids({ sceneOpacity, ...props }) {
  const { nodes, materials } = useGLTF("./models/asteroids/model.glb");
  const materialRef = useRef();

  useFrame(() => {
    materialRef.current.opacity = sceneOpacity.current;
  });
  return (
    <group {...props} dispose={null}>
      <mesh geometry={nodes.Asteroids_Mesh.geometry}>
        <meshStandardMaterial
          ref={materialRef}
          onBeforeCompile={fadeOnBeforeCompile}
          envMapIntensity={7}
          color={"brown"}
          transparent
        />
      </mesh>
    </group>
  );
}

useGLTF.preload("./models/asteroids/model.glb");

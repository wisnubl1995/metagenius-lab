import { useProgress } from "@react-three/drei";
import { usePlay } from "../contexts/Play";
import { useEffect } from "react";

export const Overlay = () => {
  const { progress } = useProgress();
  const { play, end, setPlay, setEnd, hasScroll, setHasScroll } = usePlay();

  useEffect(() => {
    const audio = document.getElementById("backgroundAudio");

    if (audio) {
      audio.play().catch((error) => {
        console.error("Autoplay prevented:", error);
      });
    }
  }, []);

  return (
    <div
      className={`overlay ${play ? "overlay--disable" : ""}
    ${hasScroll ? "overlay--scrolled" : ""}`}
    >
      <div>
        <audio id="backgroundAudio" autoPlay loop volume="0.5">
          <source src="/music.mp3" type="audio/mp3" />
          Your browser does not support the audio element.
        </audio>
      </div>
      <div
        className={`loader ${progress === 100 ? "loader--disappear" : ""}`}
      />
      {progress === 100 && (
        <div className={`intro ${play ? "intro--disappear" : ""}`}>
          <div className="logo">
            {play == true ? (
              <img src="./images/logo.png" />
            ) : (
              <img src="./images/logo-black.png" />
            )}
          </div>
          <p className="intro__scroll">Scroll to see what is going on</p>
          <button
            className="explore"
            onClick={() => {
              setPlay(true);
              document
                .getElementById("backgroundAudio")
                .play()
                .catch((error) => {
                  console.error("Autoplay prevented:", error);
                });
            }}
          >
            Initializing
          </button>
        </div>
      )}
      <div className={`outro ${end ? "outro--appear" : ""}`}>
        <p className="outro__text">Wait for us, we will be coming back...</p>
      </div>
    </div>
  );
};

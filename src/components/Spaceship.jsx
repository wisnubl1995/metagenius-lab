import React, { useRef, useState } from "react";
import { useGLTF } from "@react-three/drei";
import { useFrame } from "@react-three/fiber";

export function Spaceship(props) {
  const { nodes, materials } = useGLTF("./models/spaceship/model.glb");

  const spaceshipGroup = useRef();
  const linesRef = useRef();

  const rotationStep = 0.005;
  const maxRotation = Math.PI / 2; // Maksimum 20 derajat

  const [rotationDirection, setRotationDirection] = useState(0.3); // 1 untuk ke kiri, -1 untuk ke kanan
  const [totalRotation, setTotalRotation] = useState(0);

  // Animasi rotasi pada poros Z
  useFrame((state, delta) => {
    const newRotation = rotationDirection * rotationStep * delta;
    spaceshipGroup.current.rotation.z += newRotation;
    setTotalRotation(totalRotation + newRotation);

    // Cek apakah total rotasi melebihi 20 derajat
    if (Math.abs(totalRotation) >= maxRotation) {
      // Ubah arah rotasi
      setRotationDirection(rotationDirection * -0.3);
    }

    if (linesRef.current) {
      const points = linesRef.current.geometry.vertices;

      for (let i = 0; i < points.length; i++) {
        const angle =
          (i / points.length) * Math.PI * 2 + state.clock.elapsedTime;
        const radius = 2;

        points[i].x = Math.cos(angle) * radius;
        points[i].y = Math.sin(angle) * radius;
      }

      linesRef.current.geometry.verticesNeedUpdate = true;
    }
  });

  return (
    <group ref={spaceshipGroup} {...props} dispose={null}>
      <mesh
        geometry={nodes.Lo_poly_Spaceship_04_by_Liz_Reddington_1.geometry}
        material={materials.lambert4SG}
      />
      <mesh
        geometry={nodes.Lo_poly_Spaceship_04_by_Liz_Reddington_1_1.geometry}
        material={materials.lambert3SG}
      />
      <mesh
        geometry={nodes.Lo_poly_Spaceship_04_by_Liz_Reddington_1_2.geometry}
        material={materials.lambert2SG}
      />
    </group>
  );
}

const generateCircleVertices = (segments, radius) => {
  const vertices = [];

  for (let i = 0; i < segments; i++) {
    const theta = (i / segments) * Math.PI * 2;
    const x = Math.cos(theta) * radius;
    const y = Math.sin(theta) * radius;

    vertices.push(new THREE.Vector3(x, y, 0));
  }

  // Close the loop
  vertices.push(vertices[0].clone());

  return vertices;
};

useGLTF.preload("./models/spaceship/model.glb");
